/* eslint-disable no-undef */
const request = require("supertest");
const app = require("../../app/index");
require("dotenv").config();

describe("test api handleLogin", () => {
  it("return 201 OK", async () => {
    const email = "khansa@gmail.com";
    const password = "khansa";

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send({ email, password })
      .then((res) => {
        expect(res.statusCode).toBe(201);
        expect(res.body).toEqual(
          expect.objectContaining({
            ...res.body,
          })
        );
      });
  });

  it("return 404! (Not Registered)", async () => {
    const email = "iffat@gmail.com";
    const password = "iffat";

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send({ email, password })
      .then((res) => {
        expect(res.statusCode).toBe(404);
        expect(res.body).toEqual(
          expect.objectContaining({
            ...res.body,
          })
        );
      });
  });

  it("return 401! (Wrong Password)", async () => {
    const email = "khansa@gmail.com";
    const password = "andien";

    return request(app)
      .post("/v1/auth/login")
      .set("Content-Type", "application/json")
      .send({ email, password })
      .then((res) => {
        expect(res.statusCode).toBe(401);
        expect(res.body).toEqual(
          expect.objectContaining({
            ...res.body,
          })
        );
      });
  });
});

describe("test api handleRegister", () => {
  it("return 201 OK", async () => {
    const name = "paramarta";
    const email = `paramarta${Math.random().toString().substring(12)}@gmail.com`;
    const password = "123";

    return request(app)
      .post("/v1/auth/register")
      .set("Content-Type", "application/json")
      .send({ name, email, password })
      .then((res) => {
        expect(res.statusCode).toBe(201);
        expect(res.body).toEqual(
          expect.objectContaining({
            ...res.body,
          })
        );
      });
  });

  it("response 422 (EmailAlreadyTakenError)", async () => {
    const name = "khansa";
    const email = "khansa@gmail.com";
    const password = "khansa";

    return request(app)
      .post("/v1/auth/register")
      .set("Content-Type", "application/json")
      .send({ name, email, password })
      .then((res) => {
        expect(res.statusCode).toBe(422);
        expect(res.body).toEqual(
          expect.objectContaining({
            error: {
              name: expect.any(String),
              message: expect.any(String),
              details: {
                email: expect.any(String),
              },
            },
          })
        );
      });
  });
});

describe("GET /v1/auth/whoami", () => {
    it("should response with 200 as status code", async () => {
        const accessToken = await request(app).post("/v1/auth/login").send({
            email: "andien@gmail.com",
            password: "andien",
        });

        return request(app)
            .get("/v1/auth/whoami")
            .set("Authorization", `Bearer ${accessToken.body.accessToken}`)
            .then((res) => {
                expect(res.statusCode).toBe(200);
                expect(res.body).toEqual({
                    id: expect.any(Number),
                    name: null,
                    email: expect.any(String),
                    image: null,
                    createdAt: expect.any(String),
                    updatedAt: expect.any(String),
                });
            });
    });

    it("should response with 401 as status code", async () => {
        return request(app)
            .get("/v1/auth/whoami")
            .set("Authorization", `Bearer ${"invalidtoken"}`)
            .then((res) => {
                expect(res.statusCode).toBe(401);
                expect(res.body).toEqual({
                    error: {
                        name: expect.any(String),
                        message: expect.any(String),
                        details: null,
                    },
                });
            });
    });
});

