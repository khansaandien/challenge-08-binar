/* eslint-disable no-undef */
const request = require('supertest');
const app = require("../../app/index.js");
const dotenv = require('dotenv');
// eslint-disable-next-line no-unused-vars
const car = require('../../app/models/car.js');
dotenv.config();

describe('test api get all cars', () => {
    it('return 200 ok', (done) => {
        request(app)
        .get("/v1/cars")
        .expect('Content-Type', 'application/json; charset=utf-8')
        .expect(200, done);
    });
});

describe('test api create car', () => {
    it('return 201 created', async() => {
        const loginAuth =  {
            email: 'khansa@gmail.com',
            password: 'khansa'
        };

        await request(app)
        .post("/v1/auth/register")
        .send(loginAuth);

        const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        const carPayload = {
            name: "Honda",
            price: 1000,
            size: "small"
        };

        await request(app)
        .post("/v1/cars")
        .set("Authorization", token)
        .send(carPayload)
        .expect(201)
        .expect("Content-Type", "application/json; charset=utf-8");


    });

    it('return 401 unauthorized access', async() => {
        const loginAuth = {
            email: 'andien@gmail.com',
            password: 'andien'
        };

        await request(app)
        .post("/v1/auth/register")
        .send(loginAuth);

        const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        const carPayload = {
            name: "Honda",
            price: 1000,
            size: "small"
        };

        await request(app)
        .post("/v1/cars")
        .set("Authorization", token)
        .send(carPayload)
        .expect(401)
        .expect("Content-Type", "application/json; charset=utf-8");
    });
});

describe("POST /v1/cars/:id/rent", () => {
    it("should return 201 CREATED", async () => {
        const loginAuth =  {
            email: 'andien@gmail.com',
            password: 'andien'
        };

        await request(app)
        .post("/v1/auth/register")
        .send(loginAuth);

        const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        await request(app)
        .post("/v1/cars/10/rent")
        .set("Authorization", token)
        .send({
            rentStartedAt: "2020-01-01",
            rentEndedAt: "2020-01-02",
          })
        .expect(201)
        .expect("Content-Type", "application/json; charset=utf-8");
    });

    it("should return 401 Unauthorized if the token invalid", async () => {
        const loginAuth =  {
            email: 'khansa@gmail.com',
            password: 'khansa'
        };

        await request(app)
        .post("/v1/auth/register")
        .send(loginAuth);

        const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        await request(app)
        .post("/v1/cars/10/rent")
        .set("Authorization", token)
        .send({
            rentStartedAt: "2020-01-01",
            rentEndedAt: "2020-01-02",
          })
        .expect(401)
        .expect("Content-Type", "application/json; charset=utf-8");
    });
  });
  

describe('GET /v1/cars/:id', () => {
    it('return 200 using valid id', () => {
        request(app)
        .get("/v1/cars/1")
        .expect(200)
        .expect("Content-Type", "application/json; charset=utf-8");
    });

    // it('return 404 using invalid id', () => {
    //     request(app)
    //     .get("/v1/cars/1000")
    //     .expect(404)
    //     .expect("Content-Type", "application/json; charset=utf-8");
    // });
});

describe("DELETE /v1/cars/:id", () => {
    it("should return 204 No Content", async () => {

        const loginAuth =  {
            email: 'khansa@gmail.com',
            password: 'khansa'
        };

      const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        await request(app)
        .delete("/v1/cars/1")
        .set("Authorization", token)
        .expect(204);

    });

    it("should return 401 Unauthorized Access", async () => {

        const loginAuth =  {
            email: 'andien@gmail.com',
            password: 'andien'
        };

      const response = await request(app)
        .post("/v1/auth/login")
        .send(loginAuth);

        const token = `Bearer ${response.body.accessToken}`;

        await request(app)
        .delete("/v1/cars/1")
        .set("Authorization", token)
        .expect(401);

    });
  });
  
  


